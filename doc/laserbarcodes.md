# Laser Barcodes #

Laser Barcode and the successor Laser Barcode 2 were created by
Pioneer to enable the creation of interactive content on laserdiscs.
There are some educational titles, like [The Powers of the
President]([https://www.lddb.com/laserdisc/46601/88-4A/ABC-News-Interactive:-Powers-of-the-President)
that use these barcodes to access frame segments of the disc by using,
for example, the UC-V109BC remote control.

I spent some time to investigate how these barcodes really work.

Some players are advertised as supporting Laser Barcodes, e.g., the
LD-V8000.  There is some information at
[lddb](https://manuals.lddb.com/LD_Players/Pioneer/LD/LD-V8000/Documents/)
with examples of barcodes and also advertising material.  However, it
seems that some other players also support the protocol: the CLD-515
for example, seems to support them fully.

The barcodes themselves are [ITF Interleaved 2 of
5](https://en.wikipedia.org/wiki/Interleaved_2_of_5) barcodes.  These
barcodes contain an even number of digits 0-9.  For Laser Barcodes, it
seems that the first digit is always 2.  The last digit is a mod 10
checksum of all the preceeding digits of the code.

This is a list of the barcodes that are listed by example in a [technical bulletin](https://manuals.lddb.com/LD_Players/Pioneer/LD/LD-V8000/Documents/PIB%20%23155206%20-%20Laser%20Barcode%20Commands.pdf):

        Play            30
        Audio Off       10
        Analog Left	11
        Analog Right	12
        Analog Stereo	13
        Video Off	20
        Video On	21
        Pause           31
        Step Forward	32
        Step Reverse	33
        Reject		40
        Marker Clear	49
        Debug On	98
        Debug Off	98
        Start           41
        Digital Stereo	17
        Digital Left	15
        Digital Right	16
        Slow Forward 1	34
        Slow Forward 2	36
        Slow Forward 3	38
        Slow Forward 4	44
        Slow Reverse 1	35
        Slow Reverse 2	37
        Slow Reverse 3	38
        Slow Reverse 4	45

        Frame Search            <6 digits> 13
        Frame Segment Play      <5 digits> <5 digits> 11
        Chapter Search          <2 digits> 13
        Chapter Segment Play    <2 digits> <2 digits> 13
        Time Search             <6 digits> 13
        Time Segment Play       <5 digits> <5 digits> 13
        Special Effects Segment <5 digits> <5 digits> <2 digits> 11

In the table, the preceeding 2 is stripped, and also the checksum. In
the Special Effects Segment command, the interpretation of the two
digits specifying the effects (speed and direction of play) is
unknown. It should be relatively easy to try out, however.

The barcodes are sent over IR as a sequence of NEC protocol commands.
They share the address (A8) with other remote codes sent to Pioneer
laserdisc players.  The command sequence starts with B0.  Then, one
command per digit in the barcode is sent, the command being A\<digit>.
The command sequence then ends with B1.

For example, to send Play, that has the code 30, first compute the
barcode checksum.  It is 2 + 3 + 0 = 5, since the initial 2 is
included in the checksum.  The complete code to send is thus 2305.
Over the NEC protocol, this is encoded as the command sequence

        B0 A2 A3 A0 A5 B1

When this sequence is sent over IR, the intersymbol gap is normally
about 25 ms, but is extended to about 64 ms between repeating sybols.
I do not know the reason for this, but maybe it is to avoid problems
with echoes if an IR repeater is being used.  In any case it seems
some players (e.g., LD-V8000) filter out duplicate symbols sent too
close to each other, while other players (e.g., CLD-515) ignore this.
Initally the remote was working for the latter players but it took me
some time to figure out how to get the LD-V8000 to recognize the
codes.