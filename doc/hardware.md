
This is how I built the hardware device, with a simple "enclosure" like shown in the picture on the front page.  As stated elsewhere, the display is optional and of course you can use a breadboard instead.  With the few components, however, just soldering the parts together worked out just fine.

Materials required:

* Arduino Nano 33 BLE
* IR LED (I used an Osram SFH 4546)
* NPN transistor
* 1 k Ohm resistor
* 33 Ohm resistor
* 4 x size 0 machine screws (anything thinner than 1.7 mm), with nuts
* 4 x M3 machine screws (or around 3 mm), with nuts
* Base plate (I used a 2 mm thick some acrylic sheet)
* Distances (I used wire isolation and cuts of beer line)
* Zio Qwiic OLED Display (1.5inch, 128x128) (LCD-15890)

First solder together the connections required: see the schematic in the readme file. Since there are very few components everything can be put together without a PCB quite easily.
![Parts soldered together](pictures/20210503_220444.jpg)

Cut a sheet of acrylic and drill four small holes (2 mm or slightly
less) for screws to hold the Arduino board in place. Also, drill 4
larger holes (3 mm) to mount the display on top later.
![Acrylic with holes and screws](pictures/20210503_220656.jpg)

I made some spacers from the isolation of an electric cord:
![Spacers from isolation](pictures/20210503_220234.jpg)

Add the spacers under the Arduino:
![Four screws and some spacers in place](pictures/20210503_220729.jpg)

Secure the arduino using spacers and nuts:
![Arduino mounted](pictures/20210503_222415.jpg)

Insert four M3 screws from underneath:
![Screws inserted from underneath](pictures/20210503_222512.jpg)

More spacers, this time cut from beer tubing:
![Beer tubing spacers in place](pictures/20210503_222543.jpg)

Secure the display on top using four nuts. I had to grind the nuts
down on one side since the display is very close to the holes in the
PCB.  ![Build complete](pictures/20210503_223651.jpg)

Assembly complete:
![Another picture of completed build](pictures/20210503_223837.jpg)

What it looks like from underneath:
![Yet another picture of the completed build](pictures/20210503_223722.jpg)
