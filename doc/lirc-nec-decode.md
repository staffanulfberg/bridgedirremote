## NEC address / command from the lirc remote database ##

If a remote control is not available, one way to get the addess and
command for the keys is to have a look in the lirc remote control
database.  Here is an extract from the entry for the Pioneer
CU-CLD067:

    pre_data       0x15EA
    
      begin codes
          KEY_POWER                0x00000000000038C7
          KEY_EJECTCD              0x0000000000006897
          KEY_AUDIO                0x0000000000007887
    
Link to [complete file.](http://lirc.sourceforge.net/remotes/pioneer/CU-CLD067)

In the NEC protocol, first the address is transmitted, and then
repeated but with all bits inverted.  After this, the command is
transmitted, again followed by a repetition with all the bits
inverted. The individual bytes are sent with the least siginificant
bit first.

In lirc, however, that has common code for lots of protocols, the most
significant bit of the code from this file is sent first.  Also, the
fact that the address and command is repeated in an inverted version
is explicitly expressed for each key.

It turns out that the "pre_data" field, that is common for all keys,
corresponds to the NEC address.  The "15" part corresponds to the
actual address, but with the bits reversed.

    0x15 = 0b00010101

Flip the bits around and we can read the address the way it was intended:

    0b10101000 = 0xa8

Notice that the second part ("ea") is just the first part with all its
bits inverted.  This follows since 0x15 + 0xEA = 0xFF.

The individual keys are treated the same way: For the power key,
notice that 0x38 + 0xC7 = 0xFF. We are only interested in the first
part, "38".

    0x38 = 0b00111000

Reverse the bits to get

    0b00011100 = 0x1c

So, the power key has command code 0x1c.
