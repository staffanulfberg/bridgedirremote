# BridgedIrRemote #

BridgedIrRemote is a universal infrared remote control that was
initially created to replace long lost remote controls for Laserdisc players, but it now supports other devices as well.

The project consists of two parts: an Android app and an Arduino
project.  The Android app acts as the user interface and connects to
the an [Arduino Nano 33
BLE](https://store.arduino.cc/arduino-nano-33-ble) using Bluetooth Low
Energy (BLE).  For each key that is pressed, a command is sent to the
Arduino, that in turn pulses the light of an infrared LED.

The remotes for the following units have been implemented:
* Pioneer CLD-95 (CU-CLD052)
* Sony MSC-4000 MUSE decoder (RM-J178)
* Victor HR-W5 W-VHS player

They are not 100% complete in that a few
buttons are missing, but they work fine for most functions. Many remote codes are common between Pioneer Laserdisc player models so the CU-CLD052 can work to a varying degree for many Pioneer players.

Currently, the following protocols are implemented in the Arduino application:
* NEC (used by Pioneer and others)
* SIRC12 and SRIC15 (used by Sony)
* JVC (used by JVC, and of course, Victor)

Adding support for more remotes is straight-forward but currently
requires that the Android app is rebuilt.  If any IR protocol that is not listed above is required, the Android application also has to be modified.

Adding new protocols or new remotes is not hard. The part of the Android app that implements the actual remotes is written in JavaScript/HTML.  The Android application is written in C++ but a new protocol typically only requires about 30 lines of code or so; the hard part is to figure out how the protocol works.


## The Arduino project ##

The BLE to IR bridge is based on the Arduno Nano 33 BLE.  Apart from
the Arduino itself the project requires very few components: an
infrared LED, a transistor and two resistors.  I added a small OLED
display for debugging purposes but this is entirely optional.
Debugging over USB works just fine with minor code modifications.

![Schematic](doc/pictures/FritzingBridge_schem.png)

Notice that the LED gets its power from the 5V output of the Arduino
board.  By default the 5V output is disabled, and is enabled by
connecting two pads on the back side of the board.

If someone wants to duplicate my build pictured below, see [some notes
here.](doc/hardware.md)

![Picture of the remote bridge with a display](doc/pictures/20210602_124757.jpg)

After building the hardware, open the ArduinoBridge project in Arduino
studio.  Install/select the correct board and compile/upload the
sketch to the Arduino.

The project uses the ArduinoBLE library for Bluetooth Low Enery
communication. For the IR, I used the [Mbed
OS](https://os.mbed.com/mbed-os/) PWM support directly, which turned
out to make the implementation really small and simple.

The code for printing on the SSD1327 based OLED display is self
contained in the project, except it uses the Wire library for I2C
communication.  The code is heavily influenced by the [ssd1327 library
by Larry Bank](https://github.com/bitbank2/ssd1327), including the
font.  That library was sufficiently simple and well written that I
could actually understand how it worked quite quickly.  I added the
ability to set the starting offset of the frame buffer so that text
can be scrolled without refreshing the entire screen.


## The Android app ##

Install either Android Studio, or the Android plugin for IntelliJ
IDEA.  I've used the latter for this project since I already had
IntelliJ IDEA installed, but I would hope that the project works in
Android Studio too—after all it is more or less the same tool.

The easiest way to get the app on the phone is to just Debug or Run
the app, which will compile the project, upload the app to the phone,
and start it.  The app remains installed on the phone after debugging
or running it from the IDE.  Notice that first you have to enable the
developer options and allow usb debugging on the phone.

The app itself uses a WebView to present a few choices of
remote controls.  There are only three "real" remotes so far, and
also "NEC Generic" and "SIRC Generic" remotes that have
256/128 keys: one for each possible command.  These remotes have an
input field that sets the address to send; they are intended for
testing purposes.

There is also a "Laser Barcode 2" remote, that has buttons for all the
commands that are available in the Laser Barcode 2 examples given by
Pioneer.  Further information about Laser Barcodes is available
[here.](doc/laserbarcodes.md)

To add new remotes just edit the index.html page.  If a new protocol
is required (i.e., for something not NEC, SIRC or JVC), then some more work is required, including adding support to the Arduino code.


## The BLE protocol ##

The Arduino infrared bridge publishes a service, which has a single
characteristic that can be written to.  The data written is a byte
array, with the first byte indicating the infrared protocol to use.
NEC is 1, SIRC is 2 and 3 (for the 12 and 15 bit variants,
respectively), JVC is 4, and all other codes are reserved at the moment.  After
the first byte protocol dependent data follows.

For the NEC protocol, the data is represented by three bytes per
command to send in sequence: address/command/time.  This means that
most remote control keys are represented by a BLE write containing 4
bytes: 1, \<address>, \<command>,\<time>.  If several
address/command/time tuples are present they are sent after each
other. After each sent symbol, there is a pause so that the total per
sybol time is the specified time. (Normally, there is a pause of about
25 ms between symbols, but in LaserBarcode, if a symbol is duplicated
there is a 64 ms pause instead).

In SIRC, the data is always exactly 2 bytes so a total of 3
bytes is send over BLE.

For JVC, the data first contains the length of the start burst (this is 8.44 ms according to documentation from JVC, but I found that some remotes do not send the start burst at all, or send shorter start bursts). Next is the address, i.e., the device specific code.  The rest of the data is interpreted as commands to send to the specified address.

Notice that all communication is unencrypted, and that there is
nothing whatsover in place that protects you from your neighbors
controlling your equipment.

## Remote codes ##

In order to add more remote controls, and to fix problems with the
existing one, knowledge of what codes to send is required.  So far,
the project only supports the NEC and SIRC protocols, since that is
what I've used myself so far.  The procedure of figuring out the codes
would be similar for any other protocol, however.

If an actual remote is available, it is possible to decode the codes
it sends in a few ways:

* Use [lirc](https://www.lirc.org) or corresponding software together
  with an IR detector to check what codes are being sent by the
  different buttons.  There is a script in the tools directory that
  prints NEC codes when fed the output from ir-ctl (part of
  v4l-utils).

* Some laserdisc players show what remote command is received on the
  on screen display in service mode.

If a remote is not available, one way is to use one of the generic
remotes, that have 256/128 buttons corresponding to all of the possible
codes and try them out. For some obvious things like ejecting the disc
this is easy, but figuring out keys that are used in a more stateful
context can be very tricky.

Another possibility, again referring to lirc, is to have a look in
their database of supported remotes. Unfortunately, the format of the
remote control definition files is not very user friendly, but it is
possible to [figure out the codes](doc/lirc-nec-decode.md) from there.


## Todos and Contributing ##

I want to keep this project reasonably small and simple, but here is a
list of some things I have in mind to add or fix.  If someone
wants to contribute get in touch!

* Add a configuration option in the app for which remotes to display in the menu.  This will be handy when/if support for more remotes are added.

* Adding a jog dial to the CU-CLD052 and JVC HR-W5 remotes.

* Fixing issues with the Android app not detecting that the peer has
  disconnected.

* Adding some kind of security to the BLE protocol.

* Adding some way to reconfigure / add supported remotes without
  recompiling the Android app.  This way it could be pulished on
  Google play so users wouldn't need the Arduino Studio at all.  I
  might add the app anyway at some point but at the moment I beieve it
  is too early.  Actually, since the user interface is just a web page
  with javascript calls to send the command codes over BLE, it would
  be possible to just enter a URL and host the actual web page
  anywhere.

## Copyright ##

Copyright 2021 Staffan Ulfberg <staffan@ulfberg.se>
