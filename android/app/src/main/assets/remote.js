// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

window.addEventListener("load", function() {
    try {
        // Create the NEC generic remote keyboard (16x16 keys) -- useful for testing / finding correct codes
        {
            const genericKeys = document.getElementById('nec-generic').getElementsByClassName('remote-keys')[0];
            for (i = 0; i <= 255; i++) {
                const key = document.createElement('button')
                key.dataset['tosend'] = i;
                key.textContent = i.toString(16)
                genericKeys.appendChild(key);
            }
        }

        // Create the SIRC15 generic remote keyboard (8x16 keys) -- useful for testing / finding correct codes
        {
            const genericKeys = document.getElementById('sirc15-generic').getElementsByClassName('remote-keys')[0];
            for (i = 0; i <= 127; i++) {
                const key = document.createElement('button')
                key.dataset['tosend'] = i;
                key.textContent = i.toString(16)
                genericKeys.appendChild(key);
            }
        }

        // Set key actions
        document.querySelectorAll('.remote-keys').forEach(keyboard => {
            keyboard.querySelectorAll('button').forEach(item => {
                item.addEventListener('click', e => {
                    if (item.dataset.tosend) {
                        const wrapper = eval(keyboard.dataset.sendwrapper);
                        wrapper(e.target.dataset.tosend.toString())
                    }
                });
            });
        });

        // Add all remotes to the dropdown menu
        const menu = document.getElementById("dropdownMenu");
        document.querySelectorAll('.remote').forEach(item => {
            const a = document.createElement("a");
            a.textContent = item.dataset.displayName
            a.onclick = () => changeRemote(item.id)
            menu.appendChild(a);
        });

        changeRemote("uc-cld052")
    } catch (ee) {
    }
});

function reconnect() {
    window.nativeProxy.reconnect()
}

function sendCommand(data) {
    if (window.nativeProxy != null) {
        console.log("Sending: " + data)
        window.nativeProxy.sendCommand(data)
    } else {
        console.log("Cannot send: " + data)
    }
}

const necNormalSymbolTime = 94 // 66 + 28 (symbol time + delay)
const necDuplicateSymbolTime = 130 // 66 + 64 (symbol time + longer delay)

function sendNec(address) {
    return command => {
        const data = "nec:" + address + "," + command + "," + necNormalSymbolTime
        sendCommand(data)
    }
}

function sendSirc12(device) {
    return command => {
        const data = "sirc12:" + device + "," + command
        sendCommand(data)
    }
}

function sendSirc15(device) {
    return command => {
        const data = "sirc15:" + device + "," + command
        sendCommand(data)
    }
}

// According to the documentation, the start burst would normally be 8.4 ms, i.e., 16 x 527 us
function sendJvc(address, startBurstTime) {
    return command => {
        const data = "jvc:" + startBurstTime + "," + address + "," + command + "," + command + "," + command
        sendCommand(data)
    }
}

// For example, for setting the clock of the Victor HR-W5
function sendJvcSequence(address, data) {
    // Start burst is 2.1 ms, i.e., 4 x 527 us
    var command = 'jvc:4,' + address + "," + data.join(",")
    console.log('SendJvcSequence: ' + command)
    sendCommand(command)
}

function sendBarcode(data) {
    var checkSum = 0;
    [...data].forEach(c => checkSum += parseInt(c))
    const frame = '2' + data + ((2 + checkSum) % 10).toString()
    console.log('SendLaserBarcode: ' + frame)

    const address = '0xa8';
    var toSend = [ '0xb0' ];
    [...frame].forEach(c => toSend.push('0xa' + c))
    toSend.push('0xb1')

    var necCommand = 'nec:' + toSend.map((c, i) => {
        const delay = i < toSend.length - 1 && toSend[i + 1] === c ? necDuplicateSymbolTime : necNormalSymbolTime;
        return '0xa8,' + c + "," + delay
    }).join(";")
    sendCommand(necCommand)
}

function setStatus(c, s) {
    statusColor = c
    statusMessage = s
    document.getElementById('status').style.color = c
    document.getElementById('status').onclick = s && (() => alert(s))
}

function toggleDropdownMenu() {
    const dd = document.getElementById("dropdownMenu");
    dd.style.display = dd.style.display === "block" ? "none" : "block"
}

function changeRemote(id) {
    const remotes = document.querySelectorAll('.remote')
    remotes.forEach(item => {
        if (item.id === id) {
            item.style.display = "block"
            document.getElementById("currentRemote").textContent = item.dataset.displayName
        } else
            item.style.display = "none"
    });
    document.getElementById("dropdownMenu").style.display = "none"
}
