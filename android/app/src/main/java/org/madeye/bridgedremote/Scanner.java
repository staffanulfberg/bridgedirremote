// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

package org.madeye.bridgedremote;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.os.ParcelUuid;
import android.util.Log;

import java.util.List;

final class Scanner extends ScanCallback {
    public interface DeviceFoundCallback {
        void deviceFound(BluetoothDevice device);
    }

    private final String tag = "BridgedRemoteScan";
    private final BluetoothAdapter bluetoothAdapter;
    private final DeviceFoundCallback deviceFoundCallback;

    public Scanner(BluetoothAdapter bluetoothAdapter, DeviceFoundCallback deviceFoundCallback) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.deviceFoundCallback = deviceFoundCallback;
    }

    public void start() {
        Log.d(tag, "start");
        bluetoothAdapter.getBluetoothLeScanner().startScan(this);
    }

    public void stop() {
        Log.d(tag, "stop");
        bluetoothAdapter.getBluetoothLeScanner().stopScan(this);
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        Log.d(tag, "Found device: " + result);
        ScanRecord sr = result.getScanRecord();
        if (sr == null) {
            Log.i(tag, "ScanRecord = null");
        } else if ("IR remote bridge".equals(sr.getDeviceName())) {
            Log.i(tag, "Found IR remote bridge");
            bluetoothAdapter.getBluetoothLeScanner().stopScan(this);
            BluetoothDevice device = result.getDevice();
            if (device != null) {
                List<ParcelUuid> serviceUuids = sr.getServiceUuids();
                for (ParcelUuid i : serviceUuids)
                    Log.i(tag, "ServiceUuid: " + i);
                // TODO: check that the service uuid is correct
                deviceFoundCallback.deviceFound(device);
            }
        }
    }

    @Override
    public void onScanFailed(int errorCode) {
        Log.e(tag, "Error scanning: " + errorCode);
    }

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
        Log.i(tag,"onBatchScanResults " + results.size());
        for (ScanResult i : results)
            onScanResult(0, i);
    }
}
