// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

package org.madeye.bridgedremote;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.*;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.*;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class MainActivity
        extends AppCompatActivity
        implements Scanner.DeviceFoundCallback, ConnectionHandler.ConnectionStateChangedListener {

    public static final UUID serviceUuid = new UUID(0x0000FA0100001000L,0x800000805f9b34fbL);
    public static final UUID commandUuid = new UUID(0x0000210200001000L ,0x800000805f9b34fbL);
    private static final String tag = "BridgedRemoteMain";
    private static final String[] requiredPermissions = {
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_SCAN,
            Manifest.permission.BLUETOOTH_CONNECT,
    };

    private WebView browser;
    private BluetoothAdapter bluetoothAdapter;
    private Handler handler;
    private Scanner scanner;
    private ConnectionHandler connectionHandler;

    private String lastStatusColor = "";
    private String lastStatusMessage = "";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler(Looper.myLooper());

        browser = this.findViewById(R.id.browser);
        browser.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                setJsStatus();
            }
        });

        WebSettings settings = browser.getSettings();
        settings.setJavaScriptEnabled(true);
        browser.clearCache(true);
        browser.setWebChromeClient(new WebChromeClient()); // necessary for "alert()" to work
        browser.addJavascriptInterface(new JavascriptNativeAccess(), "nativeProxy");
        browser.loadUrl("file:///android_asset/index.html");

        final BluetoothManager bluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            setStatus("#f33", "Bluetooth disabled");
        } else {
            List<String> missingPermissions = Arrays.stream(requiredPermissions).filter(
                    s -> ContextCompat.checkSelfPermission(this, s) != PackageManager.PERMISSION_GRANTED).collect(Collectors.toList());
            if (missingPermissions.isEmpty()) {
                startScanning();
            } else {
                Log.i(tag, "Permissions missing -- requesting: " + missingPermissions.stream().reduce((a, b) -> a + ", " + b));
                registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), isGranted -> {
                    if (isGranted.values().stream().allMatch(g -> g)) {
                        Log.i(tag, "All permissions granted");
                        startScanning();
                    } else {
                        setStatus("#f33", "Permission denied");
                    }
                }).launch(missingPermissions.toArray(new String[] {}));
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bluetoothAdapter != null) {
            if (connectionHandler != null) {
                connectionHandler.stop();
                if (scanner != null)
                    Log.w(tag, "scanner non-null when connectionHandler set");
            } else {
                scanner.stop();
            }
        }
    }

    @Override
    public void deviceFound(BluetoothDevice device) {
        scanner.stop();
        scanner = null;
        connectionHandler = new ConnectionHandler(this, device);
        connectionHandler.start(getApplicationContext());
    }

    @Override
    public void connectionStateChanged(boolean connected, String peer) {
        if (connected)
            setStatus("#287AA9","Connected to " + peer);
        else {
            disconnect();
        }
    }

    private void disconnect() {
        setStatus("#888", "");
        connectionHandler.stop();
        connectionHandler = null;
        startScanning();
    }

    private void startScanning() {
        setStatus("#888", "Searching...");
        scanner = new Scanner(bluetoothAdapter, MainActivity.this);
        scanner.start();
    }

    private void setStatus(String c, String s) {
        lastStatusColor = c;
        lastStatusMessage = s;
        setJsStatus();
    }

    private void setJsStatus() {
        String url = "javascript:setStatus('" + lastStatusColor + "','" + lastStatusMessage +"');";
        Log.i(tag,"loadUrl: " + url);
        handler.post(() -> {
            browser.loadUrl(url);
        });
    }

    final class JavascriptNativeAccess {
        @JavascriptInterface
        public void reconnect() {
            if (connectionHandler != null) {
                Log.d(tag, "Calling disconnect");
                disconnect();
            } else
                Log.i(tag, "Not connected -- not calling disconnect");
        }

        @JavascriptInterface
        public void sendCommand(String s) {
            if (connectionHandler != null) {
                Log.i(tag, "Send command: " + s);
                if (s.startsWith("nec:")) {
                    String[] commands = s.substring(4).split(";");
                    byte[] toSend = new byte[1 + commands.length * 3];
                    toSend[0] = 1; // NEC
                    for (int i = 0; i < commands.length; i++) {
                        String[] addrCmdDelay = commands[i].split(",");
                        toSend[1 + i * 3] = parseByte(addrCmdDelay[0]);
                        toSend[1 + i * 3 + 1] = parseByte(addrCmdDelay[1]);
                        toSend[1 + i * 3 + 2] = parseByte(addrCmdDelay[2]);
                    }
                    connectionHandler.sendCommand(toSend);
                } else if (s.startsWith("sirc12:") || s.startsWith("sirc15:")) {
                    String[] addrCmd = s.substring(7).split(",");
                    byte[] toSend = new byte[3];
                    toSend[0] = (byte)(s.startsWith("sirc12:") ? 2 : 3); // SIRC12 / SIRC15
                    toSend[1] = parseByte(addrCmd[0]);
                    toSend[2] = parseByte(addrCmd[1]);
                    connectionHandler.sendCommand(toSend);
                } else if (s.startsWith("jvc:")) {
                    String[] data = s.substring(4).split(",");
                    byte[] toSend = new byte[1 + data.length];
                    toSend[0] = 4; // JVC
                    for (int i = 0; i < data.length; i++) {
                        toSend[1 + i] = parseByte(data[i]);
                    }
                    connectionHandler.sendCommand(toSend);
                } else {
                    Log.e(tag, "unrecognized protocol");
                }
            } else {
                Log.i(tag, "No connection for sending command: " + s);
            }
        }

        private byte parseByte(String s) {
            if (s.startsWith("0x"))
                return (byte)Integer.parseInt(s.substring(2), 16);
            else
                return (byte)Integer.parseInt(s);
        }
    }
}
