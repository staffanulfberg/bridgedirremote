// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

package org.madeye.bridgedremote;

import android.bluetooth.*;
import android.content.Context;
import android.util.Log;

import static android.bluetooth.BluetoothDevice.TRANSPORT_LE;
import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;

final class ConnectionHandler extends BluetoothGattCallback {
    public interface ConnectionStateChangedListener {
        void connectionStateChanged(boolean connected, String peer);
    }

    private final String tag = "BridgedRemoteGatt";
    private final ConnectionStateChangedListener listener;
    private final BluetoothDevice device;
    private BluetoothGatt gatt;
    private BluetoothGattCharacteristic commandCharacteristic;
    private boolean connected;

    ConnectionHandler(ConnectionStateChangedListener listener, BluetoothDevice device) {
        this.listener = listener;
        this.device = device;
    }

    public void start(Context context) {
        device.connectGatt(context, false, this, TRANSPORT_LE);
        Log.i(tag, "Called connectGatt");
    }

    public void stop() {
        if (gatt != null) {
            if (connected) {
                gatt.disconnect();
                Log.i(tag, "Called gatt.disconnect");
                connected = false;
            }
            gatt.close();
            gatt = null;
        }
    }

    void sendCommand(byte[] command) {
        if (gatt != null) {
            Log.i(tag, "Sending command");
            commandCharacteristic.setValue(command);
            gatt.writeCharacteristic(commandCharacteristic);
        } else {
            Log.i(tag, "gatt=null so not sending command");
        }
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        if (status == GATT_SUCCESS) {
            if (newState == BluetoothGatt.STATE_CONNECTED) {
                connected = true;
                listener.connectionStateChanged(true,  gatt.getDevice().getAddress());
                gatt.discoverServices();
            } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                connected = false;
                listener.connectionStateChanged(false, ""); // We count on stop being called later
            }
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        if (status == GATT_SUCCESS) {
            Log.i(tag, "onServiceDiscovered");

            BluetoothGattService service = gatt.getService(MainActivity.serviceUuid);
            if (service != null) {
                Log.i(tag, "Found the command service");
                commandCharacteristic = service.getCharacteristic(MainActivity.commandUuid);
                if (commandCharacteristic != null) {
                    Log.i(tag, "Found the command characteristic");
                    this.gatt = gatt;
                } else {
                    Log.e(tag, "command characteristic not found");
                }
            } else {
                Log.e(tag, "service=null");
            }
        } else {
            Log.w(tag, "onServiceDiscovered status=" + status);
        }
    }

    @Override
    public void onCharacteristicWrite (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        Log.i(tag,"characteristic written [" + status + "]");
    }
}