#!/bin/sh
exec scala "$0" "$@"
!#

/*
 * This tool parses the output form NecDecoder, specifically the
 * "Sequence: ..." lines and interprets the LaserBarcodes for reverse
 * engineering of the pauses.
 */

import scala.io.Source

object ParseLaserBarcodes {
  def main(args: Array[String]): Unit = {
    val barcodeRe = """Sequence: C:b0 (\d+) (.*?) C:b1\s*""".r
    val barcodeRe2 = """(C:a\d \d+ )*""".r

    Source.stdin.getLines().foreach { 
      case barcodeRe(t, seq) if t.toInt > 25 && t.toInt < 35 =>
        val sequence = seq.split(" ").toSeq.grouped(2).toSeq.map{ case Seq(s1: String, s2: String) =>
          assert(s1.startsWith("C:a"))
          val digit = s1.substring(s1.length - 1).toInt
          val t = s2.toInt
          assert(t > 25 && t < 35 || t > 60 && t < 80)
          (digit, if (t > 50) "+" else "-")
        }
        sequence.foreach { case (d, l) => print(s"$d $l ") }
        println
      case barcodeRe(t, seq) =>
        println(s"Error: barcodeRe($t, $seq)")
      case s =>
        println(s"Error: $s")
    }
  }
}

