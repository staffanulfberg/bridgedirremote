#!/bin/sh
exec scala "$0" "$@"
!#

/*
 * This tool parses the output form lirc and specifically ir-ctl.  Intended use is:
 * ir-ctl --mode2 --receive | NecDecoder.scala
 */

import scala.io.Source

object NecDecoder {
  val spaceRe = """space (\d+)""".r
  val pulseRe = """pulse (\d+)""".r
  val timeoutRe = """timeout (\d+)""".r

  sealed trait Token {
    def t: Int
  }

  case class Space(t: Int) extends Token

  case class Pulse(t: Int) extends Token

  case class Timeout(t: Int) extends Token

  def main(args: Array[String]): Unit = {
    val tokens = Source.stdin.getLines().map {
      case spaceRe(t) => Space(t.toInt)
      case pulseRe(t) => Pulse(t.toInt)
      case timeoutRe(t) => Timeout(t.toInt)
    }

    val it = tokens.iterator

    var currentSequence = Seq.empty[Either[(Int, Int), Int]] // A parsed address/command or a Space time
    do {
      val (address, command) = parse(it)
      currentSequence :+= Left((address, command))
      println(f"Address: $address (0x$address%h), command: $command (0x$command%h)")

      it.next() match {
        case Timeout(t) =>
	  println(s"Timeout $t")
          print("Sequence: ")
          currentSequence.foreach {
            case Left((a, c)) => print(f"C:$c%h ") // not printing adress
            case Right(t) => print(f"${t / 1000} ")
          }
          println()
          currentSequence = Seq.empty
	case Space(t) =>
	  println(s"Space $t")
          currentSequence :+= Right(t)
	case _ => throw new Exception("Timeout or Space expected")
      }
    } while (it.hasNext)
  }

  def parse(it: Iterator[Token]): (Int, Int) = {
    it.next() match {
      case Pulse(t) if t >= 8000 && t <= 10000 =>
      case _ => throw new Exception("Start pulse expected")
    }

    it.next() match {
      case Space(t) if t >= 4000 && t <= 5000 =>
      case _ => throw new Exception("Start space expected")
    }

    val bits = (0 until 32).map { _ =>
      it.next() match {
        case Pulse(t) if t >= 450 && t <= 650 =>
        case t@_ => throw new Exception(s"Pulse expected received $t")
      }

      it.next() match {
        case Space(t) if t >= 400 && t <= 650 => false
        case Space(t) if t >= 1450 && t <= 1800 => true
        case t@_ => throw new Exception(s"Token $t unexpected")
      }
    }

    it.next() match {
      case Pulse(t) if t >= 500 && t <= 600 =>
      case _ => throw new Exception("End pulse expected")
    }

    val addressBits = bits.slice(0, 8)
    val addressInverse = bits.slice(8, 16)
    val commandBits = bits.slice(16, 24)
    val commandInverse = bits.slice(24, 32)

    def checkInverse(a: Seq[Boolean], b: Seq[Boolean]) =
      a.lazyZip(b).foreach((a, b) => assert(a == !b))

    checkInverse(addressBits, addressInverse)
    checkInverse(commandBits, commandInverse)

    def decodeBits(bits: Seq[Boolean]) =
      (0 until 8).lazyZip(bits).map((i, b) => if (b) (1 << i) else 0).sum

    val address = decodeBits(addressBits)
    val command = decodeBits(commandBits)

    (address, command)
  }
}
