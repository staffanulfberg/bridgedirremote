function drawBarcodes() {
    const start = document.getElementById("start").value
    const suffix = document.getElementById("suffix").value
    const count = document.getElementById("count").value
    const barcodesDiv = document.getElementById('barcodes')
    barcodesDiv.innerHTML = '';
    for (i = 0; i < count; i++) {
	const code = String(parseInt(start) + i).padStart(start.length, '0') + suffix
	const checkDigit = [...code].map(c => parseInt(c)).reduce((acc, v) => acc + v) % 10
	const text = code + checkDigit
	if (text.length % 2 != 0)
	    alert("Not even length")
	console.log(text);
	try {
	    const canvas = barcodesDiv.appendChild(document.createElement('canvas'));
	    bwipjs.toCanvas(canvas, {
		bcid:        'interleaved2of5',
		text:        text,
		scale:       2,               // 3x scaling factor
		height:      10,              // Bar height, in millimeters
		includetext: true,            // Show human-readable text
		textxalign:  'center',        // Always good to set this
            })
	} catch (e) {
	    alert(e)
	}
    }
}
