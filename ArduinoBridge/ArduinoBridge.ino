#include <Arduino.h>
#include <ArduinoBLE.h>
#include <nrfx_pwm.h>
#include "ssd1327.h"
#include "console.h"
// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

#include "ir.h"
#include "protocols.h"

Ssd1327 display;
Console console(display);

BLEService irRemoteBridgeService("FA01");
#define COMMAND_CHARACTERISTIC_UUID "2102"
#define MAX_COMMAND_LENGTH 60
BLECharacteristic commandCharacteristic(COMMAND_CHARACTERISTIC_UUID, BLEWrite, MAX_COMMAND_LENGTH);

Ir ir(console);
Protocols protocols(ir, console);

void println(String s)
{
  console.println(s);
}

void setup() {
  display.init(0x3c, 400000L);
  console.clear(true);
  
  if (!BLE.begin()) 
  {
    println("Starting BLE failed!");
    while (true);
  }

  println("Our address is " + BLE.address());
    
  // set Characteristic 0x2a00 (Device Name) of Service 0x1800 (Generic Access Profile)
  BLE.setDeviceName("IR remote bridge"); 
  // set Characteristic 0x2a01 (Appearance) to BLE_APPEARANCE_GENERIC_MEDIA_PLAYER
  BLE.setAppearance(640);                   
  BLE.setLocalName("IR remote bridge");
  
  BLE.setAdvertisedService(irRemoteBridgeService);
  irRemoteBridgeService.addCharacteristic(commandCharacteristic);
  BLE.addService(irRemoteBridgeService);

  BLE.setEventHandler(BLEConnected, blePeripheralConnectHandler);
  BLE.setEventHandler(BLEDisconnected, blePeripheralDisconnectHandler);
  commandCharacteristic.setEventHandler(BLEWritten, commandCharacteristicWritten);

  BLE.advertise();
  println("BLE active, waiting...");
}

void commandCharacteristicWritten(BLEDevice central, BLECharacteristic characteristic) {
  if (characteristic.uuid() == String(COMMAND_CHARACTERISTIC_UUID))
  {
    byte command[MAX_COMMAND_LENGTH];
    int l = characteristic.readValue(command, MAX_COMMAND_LENGTH);
    
    String s = "Command: ";
    for (int i = 0; i < l; i++)
      s += String(command[i]) + " ";
    println(s);
    
    protocols.send((Protocols::Protocol)command[0], l - 1, command + 1);
  }
}

void blePeripheralConnectHandler(BLEDevice central) {
  println("Connected to " + central.address());
  digitalWrite(LED_BUILTIN, HIGH);
}

void blePeripheralDisconnectHandler(BLEDevice central) {
  println("Disconnected from " + central.address());
  digitalWrite(LED_BUILTIN, LOW);
}

void loop()
{
  BLEDevice central = BLE.central();
  if (central) 
  {
    while (central.connected()) {
      delay(100);
    }
  }
}
