// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

#ifndef __SSD1327__
#define __SSD1327__

class Ssd1327 {
  public:
    void init(int addr, int32_t iSpeed);
    
    // Sets the brightness (0=off, 255=brightest)
    void setContrast(unsigned char ucContrast);

    void power(bool on);

    // Draw a string of 6x8 characters at the given col/row with the given foreground (MSN) and background (LSN) colors
    void writeString(uint8_t x, uint8_t y, char *msg, unsigned int length, int ucFGColor, int ucBGColor);

    // Fill the frame buffer with a color, e.g., black (0x00) or white (0xf)
    void fill(unsigned char ucColor);

    void setDisplayStartLine(unsigned char startLine);

  private:
    static const unsigned char font[];
    static const int iMaxX = 128, iMaxY = 128;
    int oledAddr;

    void oledWrite(unsigned char *pData, int iLen);
    void writeCommand(unsigned char c);
    void writeCommand2(unsigned char c, unsigned char d);
    void setPosition(int x, int y, int cx, int cy);
    void writeDataBlock(unsigned char *ucBuf, int iLen);
};

#endif // __SSD1327__
