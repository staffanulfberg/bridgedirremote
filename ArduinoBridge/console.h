// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include "ssd1327.h"

class Console 
{
  private:
    static const int consoleRows = 16;
    static const int consoleCols = 21;

    Ssd1327 &display;
    char buffer[consoleRows][consoleCols];
    int currentRow;
    int currentCol;
    int topRow;
    bool startedScrolling;

    void sendCurrentRowAndAdvance()
    {
      display.writeString(0, currentRow * 8, buffer[currentRow], consoleCols, 15, 0);

      currentCol = 0;
      currentRow++;
      if (currentRow == consoleRows)
      {
        currentRow = 0;
        startedScrolling = true;
      }
      topRow = startedScrolling ? currentRow : 0;

      display.setDisplayStartLine(topRow * 8);
    }
    
  public:
    Console(Ssd1327 &d): display(d), currentRow(0), currentCol(0), topRow(0), startedScrolling(false)
    {
    }
    
    void print(String s)
    {
      for (unsigned int i = 0; i < s.length(); i++)
      {
        buffer[currentRow][currentCol] = s[i];
        currentCol++;
        if (currentCol == consoleCols)
          sendCurrentRowAndAdvance();
      }
    }

    void println(String s)
    {
      print(s);
      
      if (currentCol != 0) 
      {
        while (currentCol < consoleCols)
          buffer[currentRow][currentCol++] = ' ';
        sendCurrentRowAndAdvance();
      }
    }

    void refresh() 
    { 
      display.setDisplayStartLine(topRow * 8);
      for (int i = 0; i < consoleRows; i++)
      {
        display.writeString(0, i * 8, buffer[i], consoleCols, 15, 0);
      }
    }

    void clear(bool refresh)
    {
      currentRow = currentCol = topRow = 0;
      startedScrolling = false;
      
      for (int i = 0; i < consoleRows; i++)
        for (int j = 0; j < consoleCols; j++)
          buffer[i][j] = ' ';

      if (refresh) // somewhat cheaper than actually calling refresh()
        display.fill(0);
    }
};

#endif
