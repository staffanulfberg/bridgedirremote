// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

#ifndef __PROTOCOLS_H__
#define __PROTOCOLS_H__

#include "console.h"
#include "ir.h"
 
class Protocols {
  public:
    enum Protocol {
      NEC = 1,
      SIRC12 = 2,
      SIRC15 = 3,
      JVC = 4,
    };

    Protocols(Ir &_ir, Console &_log) : ir(_ir), log(_log) {
    }
    
    void send(Protocol protocol, unsigned int length, byte const data[]) {
      switch (protocol) {
        case NEC:
          sendNec(length, data);
          break;
        case SIRC12:
        case SIRC15:
          if (length != 2) {
            log.println("SIRC15: incorrect length " + length);
            return;
          }
          sendSirc(protocol, data[0], data[1]);
          break;
        case JVC:
          sendJvc(length, data);
          break;
        default:
          log.println("Unknown protocol " + protocol);
          break;
      }
    }

  private:
    Ir &ir;
    Console &log;

    static const unsigned int maxSequenceLength = 200;
    bool sequence[maxSequenceLength];
    unsigned int nextToWrite;

    /*
     * Each command is represented by 3 bytes: Address / Command / Total time in ms.
     */
    void sendNec(unsigned int length, byte const data[]) {
      if (length % 3 != 0) {
        log.println("NEC: incorrect length " + length);
        return;
      }

      Ir::Sequence sequence(21, log); // 21 * 1/38222 = 0.55 ms
        
      for (uint offset = 0; offset < length; offset += 3) {
        byte address = data[offset];
        byte command = data[offset + 1];
        long totalTime = data[offset + 2]; 
  
        unsigned long irData = ((unsigned long)(~command & 0xff) << 24) | ((unsigned long)command << 16) | ((~address & 0xff) << 8) | address;
  
        sequence.add(true, 16);
        sequence.add(false, 8);
        for (int i = 0; i < 32; i++) {
          sequence.add(true, 1);
          sequence.add(false, irData & (1 << i) ? 3 : 1);
        }
        sequence.add(true, 1);

        int burstsForCommand = 16 + 8 + 32 * (1 + 2) + 1; // Equally many 1 and 0, so using 2 (the average space time) is ok!
        int totalBurstTimes = totalTime / 0.55 + 0.5;
        int delayBursts = totalBurstTimes - burstsForCommand;
        if (delayBursts > 0)
          sequence.add(false, delayBursts);
        else
          log.println("non-positive delay " + String(delayBursts) + " bursts, totalTime=" + String(totalTime) + " ms = " + String(totalBurstTimes) + 
          " bursts, burstsForCommand=" + String(burstsForCommand));
      }
      ir.sendSequence(38222, 0.33, sequence);
    }

    // protocol can be SIRC12 or SIRC15
    // The data is always sent three times
    void sendSirc(Protocol protocol, byte device, byte command) {
        unsigned int irData = ((device & 0xff) << 7) | command;
  
        Ir::Sequence sequence(24, log); // 24 * 1/40000 = 0.6 ms

        int units = 0;
        sequence.add(true, 4);
        units += 4;
        sequence.add(false, 1);
        units++;
        for (int i = 0; i < (protocol == SIRC12 ? 12 : 15); i++) {
          sequence.add(true, irData & (1 << i) ? 2 : 1);
          units += irData & (1 << i) ? 2 : 1;
          sequence.add(false, 1);
          units++;
        }

        for (int i = 0; i < 3; i++) {
          ir.sendSequence(40000, 0.33, sequence);      
          delay(45- (units * 0.6)); // 45 ms total
        }
    }

    /*
     * First byte is the length of the start burst (in multiples of 0.527 ms); 0 means no start burst (and no follwing space).
     * Second byte is the address.
     * The rest of the data are commands.
     * 
     * The "standard" start burst is 8400 us, represented by 16 here.  This is optional in the specification and the remote for the
     * Victor HR-W5 doesn't send it.  That same remote sends a start burst of 2100 us before sending a sequence of commands for 
     * setting the clock.
     * 
     * Some devices requires data to be repeated three times, but this has to be done on the calling side.  When repeated, the start burst
     * is not present in the repeats, so just adding the same command three times in data results in the correct sequence.
     */
    void sendJvc(unsigned int length, byte const data[]) {
      if (length < 3) {
        log.println("JVC: incorrect length " + length);
        return;
      }

      Ir::Sequence sequence(20, log); // 20 * 1/37900 = 0.5277 ms

      int startBurstTime = data[0];
      if (startBurstTime > 0) {
        sequence.add(true, startBurstTime);
        sequence.add(false, 8);
      }

      byte address = data[1];      

      for (uint offset = 2; offset < length; offset ++) {
        byte command = data[offset];
  
        unsigned long irData = ((command & 0xff) << 8) | address;

        int totalNumberOfBursts = 0;
        for (int i = 0; i < 16; i++) {
          sequence.add(true, 1);
          int numberOfBursts = irData & (1 << i) ? 3 : 1;
          sequence.add(false, numberOfBursts);
          totalNumberOfBursts += numberOfBursts;
        }
        sequence.add(true, 1);
        sequence.add(false, 88 - totalNumberOfBursts);
      }
      
      ir.sendSequence(37900, 0.33, sequence);
    }
};

#endif
