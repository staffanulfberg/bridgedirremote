// Copyright 2021 Staffan Ulfberg staffan@ulfberg.se

#ifndef __IR_H__
#define __IR_H__

#include "console.h"

class Ir {
  public:
    /*
     * A sequence represents data to send as a batch over ir. Typically a batch would represent one transfer, since timing
     * within the batch will be perfect.  The sequence consist of a number of burst cycles, each of which can output either
     * a pulse or a space (ir modulated at the modulation freuency or turned off, respectively). The maximum length of a sequence
     * is 16384 (SEQ[n].CNT in the nRF52840 is 14 bits).
     */
    class Sequence {
      public:
        static const int maxLength = 4095; // This could be higher if necessary!
        const unsigned int burstCycles;

        Sequence(unsigned int _burstCycles, Console &_log) : burstCycles(_burstCycles), log(_log), length(0) {
        }
        
        void add(bool v, unsigned int bursts) {
          if (length + bursts > maxLength) {
            log.println("Sequence too long");
          } else {
            for (unsigned int i = 0 ; i < bursts; i++)
              sequence[length++] = v;
          }
        }

        unsigned int getLength() const {
          return length;
        }

        bool get(unsigned int i) {
          return sequence[i];
        }
        
      private:
        Console &log;
        unsigned int length;
        bool sequence[maxLength];
    };
    
    Ir(Console &_log) : log(_log) {
    }
  
    void sendSequence(double modulationFrequency, double dutyCycle, Sequence sequence) {
      uint16_t topValue = 16000000 / modulationFrequency;
      nrfx_pwm_config_t pwmConfig = {
        .output_pins  = {
          32 + 13, // Arduino pin 5
          NRFX_PWM_PIN_NOT_USED,
          NRFX_PWM_PIN_NOT_USED,
          NRFX_PWM_PIN_NOT_USED,
        },
        .irq_priority = NRFX_PWM_DEFAULT_CONFIG_IRQ_PRIORITY,
        .base_clock   = NRF_PWM_CLK_16MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = topValue,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO,
      };
      nrfx_pwm_init(&pwm, &pwmConfig, NULL);

      uint16_t offValue = topValue * (1 - dutyCycle);
      for (unsigned int i = 0; i < sequence.getLength(); i++) {
        uint16_t v = sequence.get(i) ? offValue : topValue;
        seqValues[i] = { v, 0, 0, 0 };
      }
      seqValues[sequence.getLength()] = { topValue, 0, 0, 0 }; // for some reason the last item doesn't finish completely, so add some padding
      pwmSeq.repeats = sequence.burstCycles - 1;
      pwmSeq.length = (sequence.getLength() + 1) * sizeof(nrf_pwm_values_individual_t) / sizeof(uint16_t);
      
      nrfx_pwm_simple_playback(&pwm, &pwmSeq, 1, NRFX_PWM_FLAG_STOP);

      // do not include the padding in the time calculation
      long T = (float)sequence.getLength() * sequence.burstCycles / modulationFrequency * 1000 + 0.9999; // round up
      delay(T);  

      nrfx_pwm_uninit(&pwm);
      // log.println("Delay total " + String(T));
    }

  private:
    Console &log;
    
    const nrfx_pwm_t pwm = NRFX_PWM_INSTANCE(0);
    nrf_pwm_values_individual_t seqValues[Sequence::maxLength + 1]; // room for padding
    nrf_pwm_sequence_t pwmSeq = {
      .values = { .p_individual = seqValues },
      .length = 0, // fill in again later
      .repeats = 0, // fill in later
      .end_delay = 0
    };
};

#endif
